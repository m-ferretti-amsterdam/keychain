import { LitElement, html, css, property } from 'lit-element';

export class PasswordManager extends LitElement {
  @property({ type: String }) title = 'key-chain';

  static styles = css`
    host {
      font-family: arial, helvetica, sans-serif;
    }

    .header {
      display: flex;
      justify-content: center;
      background-color: orchid;
      padding: 16px 40px;
    }

    .header__title {
      font-size: 36px;
      color: indigo;
    }
  `;

  render() {
    return html`
      <header class="header" role="banner">
          <h1 class="header__title">keychain<h1>
      </header>

      <div class="main-block">
          CIAO
      </div>
    `;
  }
}
